package com.yeejoin.precontrol.common.fileparser.product.png;

public interface AbstractPng {
  public void createPng(String inputFile, String outputFile) throws Exception;
}

