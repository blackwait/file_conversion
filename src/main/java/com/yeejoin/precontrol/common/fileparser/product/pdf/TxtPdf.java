package com.yeejoin.precontrol.common.fileparser.product.pdf;


import com.yeejoin.precontrol.common.fileparser.utils.ItextUtils;
import com.yeejoin.precontrol.common.fileparser.utils.TikaUtils;

public class TxtPdf implements AbstractPdf {

  @Override
  public void createPdf(String inputFile, String outputFile) throws Exception {
    String message = TikaUtils.parse(inputFile);
    ItextUtils.createSimplePdf(message, outputFile);
  }
}
