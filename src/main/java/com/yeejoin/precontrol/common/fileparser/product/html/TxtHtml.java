package com.yeejoin.precontrol.common.fileparser.product.html;

import com.yeejoin.precontrol.common.fileparser.utils.FileHelper;
import com.yeejoin.precontrol.common.fileparser.utils.TikaUtils;

public class TxtHtml implements AbstractHtml {

  @Override
  public void createHtml(String inputFile, String outputFile) throws Exception {
    TikaUtils.parseToHTML(inputFile, outputFile);
    FileHelper.parseH2(outputFile + ".html");
  }

}

