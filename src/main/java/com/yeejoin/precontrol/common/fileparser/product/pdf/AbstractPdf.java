package com.yeejoin.precontrol.common.fileparser.product.pdf;

public interface AbstractPdf {
  public void createPdf(String inputFile, String outputFile) throws Exception;
}

