package com.yeejoin.precontrol.common.fileparser.product.html;

public interface AbstractHtml {
  public void createHtml(String inputFile, String outputFile) throws Exception;
}

