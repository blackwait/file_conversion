package com.yeejoin.precontrol.common.fileparser.product.txt;

import com.yeejoin.precontrol.common.fileparser.utils.FileHelper;
import com.yeejoin.precontrol.common.fileparser.utils.TikaUtils;

public class RtfText implements AbstractText {

  @Override
  public void createTxt(String inputFile, String outputFile) throws Exception {
    String content = TikaUtils.parse(inputFile);
    FileHelper.writeFile(content, outputFile + ".txt");
  }


}

