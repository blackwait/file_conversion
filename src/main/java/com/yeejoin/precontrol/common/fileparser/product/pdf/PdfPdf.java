package com.yeejoin.precontrol.common.fileparser.product.pdf;


import com.yeejoin.precontrol.common.fileparser.utils.FileHelper;

public class PdfPdf implements AbstractPdf {

  @Override
  public void createPdf(String inputFile, String outputFile) throws Exception {
    FileHelper.copyFile(inputFile, outputFile, true);
  }

}

