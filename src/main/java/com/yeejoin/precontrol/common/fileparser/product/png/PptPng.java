package com.yeejoin.precontrol.common.fileparser.product.png;

import com.yeejoin.precontrol.common.fileparser.utils.ImageUtils;

public class PptPng implements AbstractPng {

  @Override
  public void createPng(String inputFile, String outputFile) throws Exception {
    ImageUtils.convertPpt2Png(inputFile, outputFile);
  }


}

