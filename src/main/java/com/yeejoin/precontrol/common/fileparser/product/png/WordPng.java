package com.yeejoin.precontrol.common.fileparser.product.png;

import com.yeejoin.precontrol.common.fileparser.product.html.WordHtml;
import com.yeejoin.precontrol.common.fileparser.utils.FileHelper;
import com.yeejoin.precontrol.common.fileparser.utils.ItextUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class WordPng implements AbstractPng {


  private static final String bg = "D:\\pic\\20150909140815.jpg";

  @Override
  public void createPng(String inputFile, String outputFile) throws Exception {
    WordHtml wordHtml = new WordHtml();
    wordHtml.createHtml(inputFile, outputFile);

    FileHelper.changeImageType(outputFile + ".html");
    FileHelper.checkHtmlEndTag(outputFile + ".html");
    ItextUtils.createPdf(outputFile + ".html", outputFile);

    ItextUtils.addWaterMark(outputFile + ".pdf", outputFile + "AddWaterMake.pdf", "正版授权", bg, 100,
        200);

    PdfPng pdfPng = new PdfPng();
    pdfPng.createPng(outputFile + ".pdf", outputFile);
  }

}

