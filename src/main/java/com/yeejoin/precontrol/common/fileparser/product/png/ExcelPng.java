package com.yeejoin.precontrol.common.fileparser.product.png;

import com.yeejoin.precontrol.common.fileparser.product.html.ExcelHtml;
import com.yeejoin.precontrol.common.fileparser.utils.FileHelper;
import com.yeejoin.precontrol.common.fileparser.utils.ItextUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExcelPng implements AbstractPng {


  @Override
  public void createPng(String inputFile, String outputFile) throws Exception {
    ExcelHtml excelHtml = new ExcelHtml();
    excelHtml.createHtml(inputFile, outputFile);

    FileHelper.mergeTable(outputFile + ".html");
    FileHelper.checkHtmlEndTag(outputFile + ".html");

    ItextUtils.createPdf(outputFile + ".html", outputFile + ".pdf");

    PdfPng pdfPng = new PdfPng();
    pdfPng.createPng(outputFile + ".pdf", outputFile);

  }

}

