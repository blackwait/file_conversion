package com.yeejoin.precontrol.common.fileparser.product.txt;

public interface AbstractText {

  public void createTxt(String inputFile, String outputFile) throws Exception;
}
