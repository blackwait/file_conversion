# 文件转换

#### 介绍
doc转换html、html转换word、以及html转换pdf等，根据开源的项目和代码简单的修改了一下内容，调整成自己比较满意的案例

![执行转换](https://images.gitee.com/uploads/images/2020/0715/231406_78b28cff_1165160.png "QQ截图20200715231146.png")


![图片数据的修改调整](https://images.gitee.com/uploads/images/2020/0715/231431_dc09fab7_1165160.png "QQ截图20200715231159.png")


![不显示图片是因为word不支持相对路径 建议使用第三方文件服务器 或者 云服务器接收图片](https://images.gitee.com/uploads/images/2020/0715/231453_5dbb9c1d_1165160.png "QQ截图20200715231348.png")
这里转换出来的图片不显示是因为 相对路径在 word中无法支持，因此建议 自行修改图中位置 接入第三方文件服务 或者自己服务器的地址